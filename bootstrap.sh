#!/usr/bin/env sh
touch ~/.zshrc_extra
cp zshrc ~/.zshrc
mkdir -p ~/.configs
for i in inputrc zshrc screenrc vimrc
do
  cp -r $i ~/.configs/
done

mkdir -p ~/bin
cp run_vim ~/bin/ 
