if v:progname =~? "evim"
  finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" Vundle stuff here.
filetype off
set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#rc()
Plugin 'gmarik/Vundle.vim'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Bundle 'git://github.com/davidhalter/jedi-vim'
Bundle 'fatih/vim-go'
Bundle 'kchmck/vim-coffee-script'
Bundle 'klen/python-mode'


filetype plugin indent on


" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
"  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif
set expandtab

au BufRead,BufNewFile *.go set filetype=go
au BufRead,BufNewFile *.go set noexpandtab
au BufRead,BufNewFile *.go set shiftwidth=2
au BufRead,BufNewFile *.go set tabstop=2

au BufRead,BufNewFile *.wiki set shiftwidth=4
au BufRead,BufNewFile *.wiki set expandtab
au BufRead,BufNewFile *.wiki set smartcase
au BufRead,BufNewFile *.wiki set tabstop=4
au BufRead,BufNewFile *.wiki set smartindent

au BufRead,BufNewFile *.wiki syntax match t4 "\v^\s{4}\S.*$"
au BufRead,BufNewFile *.wiki highlight link t4 T4
au BufRead,BufNewFile *.wiki syntax match t8 "\v^\s{8}\S.*$"
au BufRead,BufNewFile *.wiki highlight link t8 T8
au BufRead,BufNewFile *.wiki syntax match t12 "\v^\s{12}\S.*$"
au BufRead,BufNewFile *.wiki highlight link t12 T12
au BufRead,BufNewFile *.wiki syntax match t16 "\v^\s{16}\S.*$"
au BufRead,BufNewFile *.wiki highlight link t16 T16
au BufRead,BufNewFile *.wiki syntax match t20 "\v^\s{20}\S.*$"
au BufRead,BufNewFile *.wiki highlight link t20 T20
au BufRead,BufNewFile *.wiki syntax match t24 "\v^\s{24}\S.*$"
au BufRead,BufNewFile *.wiki highlight link t24 T24

au BufRead,BufNewFile *.wiki highlight T4 ctermfg=darkgreen guifg=darkgreen
au BufRead,BufNewFile *.wiki highlight T8 ctermfg=lightblue guifg=lightblue
au BufRead,BufNewFile *.wiki highlight T12 ctermfg=yellow guifg=yellow
au BufRead,BufNewFile *.wiki highlight T16 ctermfg=red guifg=red
au BufRead,BufNewFile *.wiki highlight T20 ctermfg=darkgray guifg=darkgray
au BufRead,BufNewFile *.wiki highlight T24 ctermfg=darkred guifg=darkred

au BufRead,BufNewFile *.html set shiftwidth=2
au BufRead,BufNewFile *.html set tabstop=2

au BufRead,BufNewFile *.js set shiftwidth=2
au BufRead,BufNewFile *.js set tabstop=2
filetype indent on

au BufRead,BufNewFile *.py noremap <C-K> :PymodeLintAuto<CR>
au BufRead,BufNewFile *.py set nonu
au BufRead,BufNewFile *.py set shiftwidth=2
au BufRead,BufNewFile *.py set tabstop=2

"au FileType go autocmd BufWritePre <buffer> Fmt
let g:pymode_lint_on_write = 0
let g:pymode_folding = 0
let g:pymode_lint_ignore = "E111"
let g:pymode_rope_lookup_project = 0

set dir=~/.swp

let mapleader=","

function! WinMove(key)
    let t:curwin = winnr()
    exec "wincmd ".a:key
    if (t:curwin == winnr())
        if (match(a:key,'[jk]'))
            wincmd v
        else
            wincmd s
        endif
        exec "wincmd ".a:key
    endif
endfunction

"" Move to windows with ctrl-<movement>
"" map <c-j> <c-w>j
"map <c-j> :call WinMove('j')<CR>
"map <c-k> :call WinMove('k')<CR>
"map <c-h> :call WinMove('h')<CR>
"map <c-l> :call WinMove('l')<CR>
"" Make these all work in insert mode too ( <C-O> makes next cmd
"" happen as if in command mode )
"" imap <C-W> <C-O><C-W>
"" This will fuck up ctrl-w being able to remove previous word

function! LogConceptSkip(conceptString)
    let appendList = [a:conceptString]
    let currLogInfo = readfile("/home/jz/.vim/concept_skips.log")
    call writefile(currLogInfo + appendList, "/home/jz/.vim/concept_skips.log")
endfunction

function! ExtractConceptStringFromLink(textWithLink)
    " Get the first set of text between brackets on line
    " E.g. {This String}
    return matchstr(a:textWithLink, '{\zs[^{]\+\ze}')
endfunction

function! JumpToConceptMatch(conceptString)
    " Note: I tried to use the yank command to move it to the " register but I
    " couldn't get it to work

    " Set the searchregister to find occurences of string that doesn't have a
    " bracket before it and ensure neither a non-space character right after
    " OR space then non-space character doesn't follow.
    " lookbehind in vim:
    "   https://jbodah.github.io/blog/2016/11/01/positivenegative-lookaheadlookbehind-vim/
    let @/ = '\({\)\@<!' . a:conceptString . '\( \S\|\S\)\@!'

    " Log a jump event
    call LogConceptSkip(a:conceptString)

    " Jump to top of buffer and go to next match in search register
    exe "normal! ggn"
endfunction

function! GotoFirstConceptMatch()
    let conceptString = ExtractConceptStringFromLink(getline('.'))
    call JumpToConceptMatch(conceptString)
endfunction
nnoremap <leader>* :call GotoFirstConceptMatch()<CR>

nnoremap <Down> :call search('^\s\s\s\s\S')<CR>
nnoremap <Up> :call search('^\s\s\s\s\S', 'b')<CR>

function! HandleConceptMenuSelection(e)
    echom a:e
    call JumpToConceptMatch(a:e)
endfunction

function! LRUConceptMenu()
    " cat -n file_name | sort -uk2 | sort -nk1 | cut -f2-
    " XXX: need to touch ~/.vim/concept_skips.log
    call fzf#run({'source': 'tac ~/.vim/concept_skips.log | cat -n | sort -uk2 | sort -nk1 | cut -f2-', 'sink': function('HandleConceptMenuSelection'), 'down': '50%', 'options': '--reverse'})
endfunction
nmap ,d :call LRUConceptMenu()<CR>

function! ExtractSubConcept()
    " Extract everything starting from first non-space character
    let conceptString = matchstr(getline('.'), '\S.*$')

    " Get the number of leading spaces on this line
    " Note: Should redo the pattern with single quotes
    let numSpaces = len(matchstr(getline('.'), "^\\s\\+"))

    " Place a concept link a line above the selection
    call append(getpos(".")[1] - 1, repeat(" ", numSpaces) . "{" .  conceptString . "}")

    " Replace leading spaces in selection
    execute "'<,'>s/^" . repeat(" ", numSpaces) . "//g"

    " Cut selection lines (now without leading spaces)
    execute "'<,'>d"

    " Jump to beginning of origin concept (line that starts with non-space character)
    call search('^\S', 'b')

    " Add a blank line above it
    call append(getpos(".")[1] - 1, "")

    " Go to added blank line and paste cut selection text
    normal! kP

    call LogConceptSkip(conceptString)
endfunction
" Ctrl U (<C-U>) removes the range('<,'>) part that vmap adds automatically
vmap ,e :<C-U>call ExtractSubConcept()<CR>

function! EmbeddedConceptsMenu()
    " Jump to beginning of origin concept (line that starts with non-space character)
    " Get position
    call search('^\S', 'b')
    let conceptStart = getpos('.')[1]

    " Jump to end of origin concept (next line that starts with non-space character)
    " Get position
    call search('^\S')
    let conceptEnd = getpos('.')[1] - 1

    " Go through each line and find any links and their corresponding concept
    " texts
    let conceptLines = getline(conceptStart, conceptEnd)
    let concepts = []
    for conceptLine in conceptLines
        if (len(ExtractConceptStringFromLink(conceptLine)) > 0)
            call add(concepts, ExtractConceptStringFromLink(conceptLine))
        endif
    endfor

    " Open menu
    call fzf#run({'source': concepts, 'sink': function('HandleConceptMenuSelection'), 'down': '50%', 'options': '--reverse'})
endfunction
nmap ,D :call EmbeddedConceptsMenu()<CR>
