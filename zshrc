#
# .zshrc is sourced in interactive shells.
# It should contain commands to set up aliases,
# functions, options, key bindings, etc.
#

export XDG_CONFIG_HOME=$HOME/.configs

fpath=($HOME/.zsh_crap $fpath)

autoload -U compinit
compinit

#allow tab completion in the middle of a word
setopt COMPLETE_IN_WORD

HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory extendedglob
setopt nobeep
setopt correct
bindkey -v
bindkey '\e/' vi-history-search-backward
bindkey '\e,' vi-history-search-forward

export EDITOR=/usr/bin/vim

export PAGER=/usr/bin/less

export EDITOR=vim


xterm_title()
{
    builtin print -n -P -- "\e]0;$@\a"
}
screen_title()
{
#    builtin print -n -P -- "\ek$@\e\\"
    xterm_title "$@"
}

case $TERM in
    screen|screen-w|screen-bce)
        alias titlecmd="screen_title"
    ;;
    xterm)
        alias titlecmd="xterm_title"
    ;;
    *)
        alias titlecmd=":"
    ;;
esac

function preexec ()
{
    titlecmd "%m %2~" ":" "\"$1\""
}

function precmd ()
{
    titlecmd "%m %2~"
}

function ssh()
{
    titlecmd "$1";
    command ssh $*;
    titlecmd "$HOSTNAME";
}

function v()
{
    cmd=$(~/bin/run_vim $1)
#    echo $cmd
    eval $cmd
}

svn-add-unknown()
{
  svn stat --no-ignore | perl -lne 'print if s/^[I\?]\s*//;' | xargs svn add 
}

function parse_git_branch() {
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  echo "["${ref#refs/heads/}"]"
}

#set the prompt and right prompt.
setopt PROMPT_SUBST
PROMPT='[%n@%m]%# '
RPROMPT='%~ $(parse_git_branch)'

zstyle ':completion:*:*:*:*:processes' menu yes select
zstyle ':completion:*:*:*:*:processes' force-list always

zstyle ':completion:*:processes-names' command  'ps c -u ${USER} -o command | uniq'

## keep background processes at full speed
#setopt NOBGNICE
## restart running processes on exit
#setopt HUP

## history
#setopt APPEND_HISTORY
## for sharing history between zsh processes
#setopt INC_APPEND_HISTORY
#setopt SHARE_HISTORY

## never ever beep ever
#setopt NO_BEEP

## automatically decide when to page a list of completions
#LISTMAX=0

## disable mail checking
#MAILCHECK=0

# autoload -U colors
#colors

alias ls='ls -GF --color'
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias c="clear"
alias la="ls -a"
alias dirs='dirs -v'
alias pu='pushd'
alias po='popd'
alias les=less
alias g4='git'

sudo() { }
unfunction sudo
export INPUTRC=~/.inputrc

export PATH=$PATH:$HOME/bin:$HOME/go/bin
export GOPATH=~/go

set -o vi

source ~/.zshrc_extra
